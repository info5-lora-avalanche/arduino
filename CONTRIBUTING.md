# Contributing rules to the Saint-Bernard project
1. [Format of the commit message](#commit)
2. [Branches](#branches)
3. [Merge](#merge)

## Format of the commit message <a name="commit"></a>

```
#Ticket_number [type] <description>

```
#### Type
At the beginning of each commit name, please add one of these tags:
* `[FEAT]`: for a new feature
* `[CPTN]`: for a new component
* `[BUILD]`: for changes that affect the build system or external dependencies
* `[BUG]`: for reporting a bug
* `[FIX]`: for error correction
* `[DOC]`: for writing or updating documentation
* `[CLEAN]`: for refactoring the code
* `[TEST]`: for adding or rewriting tests
* `[COMMENT]`: for adding comment
* `[CORRECTION]`: for a correction of the previous code
* `[WIP]`: stand for Work In Progress. Use it to make a save of your work, or if no other flag match with your commit
* `[MERGE]`: for merging a branch into another

#### Description
* use imperative, present tense: "change" not "changed" nor "changes"
* no dot (.) at the end
* summarize what you did

#### Example
```
git commit -m "#11 [FEAT] New feature done"
```

## Branches<a name="branches"></a>
### Format of the branch
```
<n°_issue> - <keywords>
```
### Rules
* The `master` branch should **only** be used for __**the last stable release and validated by the customer**__
* The `dev` branch is for the lastest code version

#### Creation of a new branch if
* You start a new ticket, the branch name has the same name of the ticket's title
* You want to merge a branch with `master` or `dev`, to test if the merge is successful. This branch does not necessarily need to be on the git server.



## Merge<a name="merge"></a>
* Merge every sub-branches with `dev` before merging it with `master`.
* When the creation of a release is possible : Merge `dev` with `master`.

:warning: After that, you have to:
* Create a tag from `master`
* Update CHANGELOG.md: add at the current date the new features and updates included in the release.

