# Saint-Bernard :: Contributors

## Dev Team
### 2021
* [Alexandra Chaton](https://gitlab.com/encre) - Scrum Master (INFO5, Polytech Grenoble)
* [Thomas Frion](https://gitlab.com/Timmy73) - Project Leader (INFO5, Polytech Grenoble)
* [Romain Pasdeloup](https://gitlab.com/Arxwel) - Git Master (INFO5, Polytech Grenoble)
### 2020
* Leya Badat - Project Leader (INFO5, Polytech Grenoble)
* Victor Cuau (INFO5, Polytech Grenoble)
* Jérémy Masson - Scrum Master (INFO5, Polytech Grenoble)
* Damien Wykland (INFO5, Polytech Grenoble)
* Paul Zarcos (INFO5, Polytech Grenoble)

## Project sponsors
* Bernard Tourancheau (Université Grenoble Alpes, Polytech Grenoble, LIG)
* Olivier Favre (PGHM Isère)

