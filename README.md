# Saint-Bernard project (chip code)

> March 17th, 2021 - Copyright (c) 2021 (License MIT)

_To see further informations about the project go to the [Documentation repository](https://gitlab.com/info5-lora-avalanche/documentation)._

## Introduction

In France, each year we count an average of 15,000 requests for mountain rescue. In Isère, the average number of mountain rescues per year is 400. Mountain rescue is divided between several services and associations: Peloton de Gendarmerie de Haute Montagne (PGHM), CRS Alpes, Groupement de Montagne des Sapeurs Pompiers (GMSP), Association Nationale des Médecins et Sauveteurs en Montagne (ANMSM) and the SAMU.

The greatest difficulty in mountain rescue is locating the victim. Sometimes, the very fact of knowing that a person needs to be rescued is a real problem, especially in white zones.

Our project, named Saint-Bernard and formerly called LoRa-valanche, aims to address both of these issues.

#### Our goals
We want to improve the rescue success rate by making a tool which is more accessible, effective and efficient.

We want this solution to be:
* **simple:** usable by all types of users
* **precise:** more precise than an avalanche transceiver
* **affordable:** a solution at reduced cost

#### For whom?
Our work is done  in patrnership with the Peloton de Gendarmerie de Haute Montagne.
Our tool is made for:
* **rescuers** as a complement to an avalanche transceiver
* **people going** to the mountains who are potential victims.

#### How does it work?
This tool makes it possible to send, at a predefined frequency, the user's GPS coordinates to a remote server. The transmission is done through the mobile network (3g & 4g). The limit of this solution is of course the white zones in the mountains.

## Presentation

The project is a fork of last year's (2020) version of the project. This version of the embedded code implements features that were not written in the previous version. Here are some additional features:
- Packet encoder (and decoder)
- Handling of multiple operation modes
- The chip will try to get the RSSI of the last LoRa signal. If the RSSI is no available, the chip will get the SNR.

This new version contains the code of the previous version but improved (refactored and cleaned).
The previous version is available here: https://gitlab.com/info5_2020_secoursenmontagne/arduino

The authors of the previous version were :
- Leya BADAT (Project leader)
- Victor CUAU 
- Jérémy MASSON
- Damien WYKLAND
- Paul ZARCOS

### Important

The code has been developed with hardware:
- Sodaq Explorer
- Bluetooth Module HC-05

## Requirements

To work on this project you need to have some requirements:
- Arduino knowledge
- Wireless communication knowledge (LoRa & BLE)
- Knowledge of C/C++ (especially comfortable with pointers)

## Authors and Contributors of 2021 version

**Authors**
- Alexandra CHATON alexandra.chaton@etu.univ-grenoble-alpes.fr (Scrum master)
- Romain PASDELOUP romain.pasdeloup@etu.univ-grenoble-alpes.fr (Git master)
- Thomas FRION thomas.frion@etu.univ-grenoble-alpes.fr (Project leader)

**Contributors**
- Ulysse COUTAUD ulysse.coutaud@univ-grenoble-alpes.fr (Teacher)
- Bernard TOURANCHEAU bernard.tourancheau@univ-grenoble-alpes.fr (Teacher in charge)
- Olivier FAVRE (PHGM Member)

## How to connect the Bluetooth HC-05 module to Sodaq Explorer Chip

The following diargam explains how to connect the different pins:

![](doc/connection-diag.png)

## Releases

- 2021-03-17: [v2.0.0-beta](https://gitlab.com/info5-lora-avalanche/arduino/-/releases/v2.0.0-beta)
