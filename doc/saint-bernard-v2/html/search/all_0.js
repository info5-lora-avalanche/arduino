var searchData=
[
  ['bbuff_0',['bbuff',['../saint-bernard-v2_8ino.html#a871a01a53c8eb9de00b412feed95c164',1,'saint-bernard-v2.ino']]],
  ['ble_5fencode_1',['ble_encode',['../packet__encoder_8c.html#aac62d922621b9a8320fa382f64bd14db',1,'ble_encode(st_bernart_packet_t pck, int rssi, char *ble_msg):&#160;packet_encoder.c'],['../packet__encoder_8h.html#aac62d922621b9a8320fa382f64bd14db',1,'ble_encode(st_bernart_packet_t pck, int rssi, char *ble_msg):&#160;packet_encoder.c']]],
  ['ble_5fserial_2',['BLE_SERIAL',['../constants_8h.html#a9fde2bf6ebea6397f5bfe36fa5ea08fb',1,'constants.h']]],
  ['blink_5fred_5fled_3',['blink_red_led',['../saint-bernard-v2_8ino.html#ac00214f8103df9ecf4041bfab874d1ff',1,'saint-bernard-v2.ino']]],
  ['bluetooth_5fsetup_4',['bluetooth_setup',['../saint-bernard-v2_8ino.html#ab0f59e16aa1fa8e5da79fae973fa0626',1,'saint-bernard-v2.ino']]]
];
