var indexSectionsWithContent =
{
  0: "bcdefghilmnprstu",
  1: "s",
  2: "cpsu",
  3: "bcdefghilprs",
  4: "bclmprs",
  5: "s",
  6: "t",
  7: "nrs",
  8: "bcdflmpst",
  9: "s"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Pages"
};

