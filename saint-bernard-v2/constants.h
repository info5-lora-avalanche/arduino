/**
 * @file constants.h
 * @author Alexandra CHATON (alexandra.chaton@etu.univ-grenoble-alpes.fr)
 * @author Romain PASDELOUP (romain.pasdeloup@etu.univ-grenoble-alpes.fr)
 * @author Thomas FRION (thomas.frion@etu.univ-grenoble-alpes.fr)
 * @brief This file contians alls useful constants
 * @version 0.1
 * @date 2021-03-16
 * @copyright Copyright (c) 2021
 */
#ifndef _CONSTANTS_H_
#define _CONSTANTS_H_

/**
 * @brief Maximum charcount to avoid writing outside of string
 */
#define MAX_DATA_SIZE 100
/**
 * @brief LoRa frequency
 */
#define FREQENCY 868500000

/**
 * @brief To activate the debug output
 */
#define DEBUG 1

/**
 * @brief The LoRa port
 */
#define LORA_SERIAL Serial2
/**
 * @brief The console to debug
 */
#define DEBUG_SERIAL SerialUSB

/**
 * @brief The Bluetooth port
 */
#define BLE_SERIAL Serial

/**
 * @brief Pin to get the bluetooth connection status
 * 
 * We have connected pin D4 of the sodaq to the State pin of the bluetooth module.
 * This connection aims to detect if the module is connected.
 */
#define PIN_BLE_STATE (4u)

/**
 * @brief Pin to turn configuration mode on/off
 * 
 * We have connected pin D8 of the sodaq to the Key pin of the bluetooth module. 
 * We do that because the D8 pin can be used for PWM.  
 * We activate the D8 pin as output.
 */
#define PIN_BLE_CONFIG_MODE (8u)

/**
 * @brief Transmitter power -3
 */
#define TX_POWER_3NEG (-3)
/**
 * @brief Transmitter power -2
 */
#define TX_POWER_2NEG (TX_POWER_3NEG + 1)
/**
 * @brief Transmitter power -1
 */
#define TX_POWER_1NEG (TX_POWER_3NEG + 2)
/**
 * @brief Transmitter power 0
 */
#define TX_POWER_0    (TX_POWER_3NEG + 3)
/**
 * @brief Transmitter power 1
 */
#define TX_POWER_1    (TX_POWER_3NEG + 4)
/**
 * @brief Transmitter power 2
 */
#define TX_POWER_2    (TX_POWER_3NEG + 5)
/**
 * @brief Transmitter power 3
 */
#define TX_POWER_3    (TX_POWER_3NEG + 6)
/**
 * @brief Transmitter power 4
 */
#define TX_POWER_4    (TX_POWER_3NEG + 7)
/**
 * @brief Transmitter power 5
 */
#define TX_POWER_5    (TX_POWER_3NEG + 8)
/**
 * @brief Transmitter power 6
 */
#define TX_POWER_6    (TX_POWER_3NEG + 9)
/**
 * @brief Transmitter power 7
 * 
 */
#define TX_POWER_7    (TX_POWER_3NEG + 10)
/**
 * @brief Transmitter power 8
 */
#define TX_POWER_8    (TX_POWER_3NEG + 11)
/**
 * @brief Transmitter power 9
 */
#define TX_POWER_9    (TX_POWER_3NEG + 12)
/**
 * @brief Transmitter power 10
 */
#define TX_POWER_10   (TX_POWER_3NEG + 13)
/**
 * @brief Transmitter power 11
 */
#define TX_POWER_11   (TX_POWER_3NEG + 14)
/**
 * @brief Transmitter power 12
 */ 
#define TX_POWER_12   (TX_POWER_3NEG + 15)
/**
 * @brief Transmitter power 13
 */ 
#define TX_POWER_13   (TX_POWER_3NEG + 16)
/**
 * @brief Transmitter power 14
 */ 
#define TX_POWER_14   (TX_POWER_3NEG + 17)

/**
 * @brief Spreading factor sf7
 */
#define SF_7  (7)
/**
 * @brief Spreading factor sf8
 */
#define SF_8  (SF_7 + 1)
/**
 * @brief Spreading factor sf9
 */
#define SF_9  (SF_7 + 2)
/**
 * @brief Spreading factor sf10
 */
#define SF_10 (SF_7 + 3)
/**
 * @brief Spreading factor sf11
 */
#define SF_11 (SF_7 + 4)
/**
 * @brief Spreading factor sf12
 */
#define SF_12 (SF_7 + 5)

#define TIMEOUT 200

/**
 * @brief Prefix of successful received message
 */
const char *RECEP_SUCCESSED  = "radio_rx";
/**
 * @brief Message when the reception failed
 */
const char *RECEP_FAILED     = "radio_err";

/**
 * @brief Error message when a LoRa chip command failed 
 */
const char *LORA_SYS_CMD_ERR = "invalid_param";

/**
 * @brief Coding rate 4/5
 */
const char *CR_5 = "4/5";
/**
 * @brief Coding rate 4/6
 */
const char *CR_6 = "4/6";
/**
 * @brief Coding rate 4/7
 */
const char *CR_7 = "4/7";
/**
 * @brief Coding rate 4/8
 */
const char *CR_8 = "4/8";

#endif // _CONSTANTS_H_
