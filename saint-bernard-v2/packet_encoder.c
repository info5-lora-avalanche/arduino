/**
 * @file packet_encoder.c
 * @author Alexandra CHATON (alexandra.chaton@etu.univ-grenoble-alpes.fr)
 * @author Romain PASDELOUP (romain.pasdeloup@etu.univ-grenoble-alpes.fr)
 * @author Thomas FRION (thomas.frion@etu.univ-grenoble-alpes.fr)
 * @brief Source code of the Saint-Bernard packet encoder
 * @version 0.1
 * @date 2021-03-16
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "packet_encoder.h"

/**
 * @brief Number of decimal for GPS Coordinates
 */
#define COORD_PRECISION 4

st_bernart_packet_t create_a_packet() {
    st_bernart_packet_t packet = malloc(sizeof(struct st_bernart_packet));
    packet->mac_addr = (char *) calloc(13, sizeof(char));
    packet->lat = 0;
    packet->lng = 0;
    packet->mode = NOMINAL;
    packet->counter = 0;

    return packet;
}

void encode(st_bernart_packet_t pck, char *output) {
    char tmp[7];

    string_2_hex("/*-", output); // We add our header (encoded in hexa)
    sprintf(output + strlen(output), "%02x", pck->mode); // we add the operation mode of the application
    sprintf(output + strlen(output), "%s", pck->mac_addr); // We add the bluetooth module mac address
    
    dtostrf(pck->lng, 3, 4, tmp); // We convert the longitude (float) into a string
    string_2_hex(tmp, (output + strlen(output))); // We add the longitude string (hexa encoded)
    
    dtostrf(pck->lat, 2, 4, tmp); // We convert the latitude (float) into a string
    string_2_hex(tmp, (output + strlen(output))); // We add the latitude string (hexa encoded)

    sprintf(output + strlen(output), "%04x", pck->counter); // We add the cpacket id
}

void ble_encode(st_bernart_packet_t pck, int rssi, char *ble_msg) {
    sprintf(ble_msg, "%s;", pck->mac_addr);
    sprintf(ble_msg + strlen(ble_msg), "%.4f;", pck->lng);
    sprintf(ble_msg + strlen(ble_msg), "%.4f;", pck->lat);
    sprintf(ble_msg + strlen(ble_msg), "%i", rssi);
}

st_bernart_packet_t decode(char *packet) {
    st_bernart_packet_t data = create_a_packet();
    // We extract the mode and the mac address from packet string
    sscanf(packet, "%02x%12s", &(data->mode), data->mac_addr);
    packet += 14; // We move forward in the string

    // We compute the lenght took by the both hex encoded GPS coordinates.
    // For that we take the total length of the packet string and remove the hexa encoded counter size
	int coords_hex_length = strlen(packet) - 5;
    // We extract the both coordinate from the packet string
    char hex_tmp[coords_hex_length];
    strncpy(hex_tmp, packet, coords_hex_length);
	hex_tmp[coords_hex_length - 1] = '\0';

    char *tmp = (char *) calloc(strlen(hex_tmp) / 2, sizeof(char));
    int ptmp = tmp;
    
    hex_2_string(hex_tmp, tmp);
    // We extract the real value of the both coordinates
    data->lng = get_float_from_string(&tmp, COORD_PRECISION);
	data->lat = get_float_from_string(&tmp, COORD_PRECISION);
    free(ptmp);
    
    packet += strlen(hex_tmp);
    sscanf(packet, "%4x", &(data->counter));
	
    return data;
}

int is_a_stb_packet(char *pck) {
    char hex_header[6], header[3];
    strncpy(hex_header, pck, 6);
    hex_2_string(hex_header, header);

    return strcmp(header, "/*-");
}

void destroy_packet(st_bernart_packet_t pck) {
    if(pck) {
        free(pck->mac_addr);
        free(pck);
    }
}