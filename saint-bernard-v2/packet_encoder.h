/**
 * @file packet_encoder.h
 * @author Alexandra CHATON (alexandra.chaton@etu.univ-grenoble-alpes.fr)
 * @author Romain PASDELOUP (romain.pasdeloup@etu.univ-grenoble-alpes.fr)
 * @author Thomas FRION (thomas.frion@etu.univ-grenoble-alpes.fr)
 * @brief Saint-Bernard packet encode/decoder declarations
 * @version 0.1
 * @date 2021-03-16
 * 
 * @copyright Copyright (c) 2021
 * The file contains every declaration of functions, structures, type definitions for handling St-Bernard packet
 */

#ifndef _PACKET_ENCODER_H_
#define _PACKET_ENCODER_H_

#include "utils.h"
#include <avr/dtostrf.h>

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>

/**
 * @struct st_bernart_packet
 * @typedef *st_bernart_packet_t
 * @brief The structure which represents a St-Bernart package (LoRa Package)
 */
typedef struct st_bernart_packet {
    T_CHIP_MODE  mode;      /**< Operation mode of the chip */
    unsigned int counter;   /**< Packet id */
    char         *mac_addr; /**< Bluetooth module mac address */
    float        lng;       /**< Longitude of GPS coordinates */
    float        lat;       /**< Latitude of GPS coordinates */
} *st_bernart_packet_t;

/**
 * @fn st_bernart_packet_t create_a_packet()
 * @brief Create a package object
 * 
 * @return A st-bernard package object, null if the objec couldn't be created
 */
st_bernart_packet_t create_a_packet();

/**
 * @fn st_bernart_packet_t decode(char *packet)
 * @brief This function aims to convert a string into a st-bernard package object
 * 
 * @param packet The hexa encoded string to convert
 * @return st_bernart_packet_t A st-bernard package object
 */
st_bernart_packet_t decode(char *packet);

/**
 * @fn void encode(st_bernart_packet_t pck, char *output)
 * @brief Transforms a st-bernard package object into string 
 * 
 * @param[in] pck Pointer to the st-bernard packet object to convert
 * @param[out] output Pointer to the output variable
 */
void encode(st_bernart_packet_t pck, char *output);

/**
 * @fn void ble_encode(st_bernart_packet_t pck, int rssi, char *ble_msg)
 * @brief Converts a St-Bernard packet into a string for BLE
 * 
 * @param[in] pck Pointer to the packet to convert and format
 * @param[in] rssi The quality of the last received LoRa signal
 * @param[out] ble_msg Pointer to the output variable
 * 
 * Converts a St-Bernard packet into a string for the mobile app. 
 * The string will contain the quality of the last received LoRa signal.
 * The string will be send through the bluetooth.
 */
void ble_encode(st_bernart_packet_t pck, int rssi, char *ble_msg);

/**
 * @fn void destroy_packet(st_bernart_packet_t pck)
 * @brief Cleans the memory of st-bernard package
 * 
 * @param pck The package to destroy
 */
void destroy_packet(st_bernart_packet_t pck);

/**
 * @fn static inline void set_mac_addr(st_bernart_packet_t pck, const char *mac)
 * @brief Set the mac addr object
 * 
 * @param pck Package which mac address should be set
 * @param mac The mac address value
 */
static inline void set_mac_addr(st_bernart_packet_t pck, const char *mac) {
    strcpy(pck->mac_addr, mac);
}

/**
 * @fn static inline float get_float_from_string(char **src, int precision)
 * @brief Extracts the fist float of the string.
 * 
 * @param[in] src Pointer of pointer to the source string
 * @param precision The precsission of the float (number of decimal)
 * @return The first fload found in the string
 * @sa extract_float_from_ble_msg
 * 
 * Compare to extract_float_from_ble_msg, this method handles strings with no seperator of any kind.
 * The src string must starts by a float
 */
static inline float get_float_from_string(char **src, int precision) {
    // We look for the fist occurence of '.' (like an index of)
    // and add to the index the float precission. 
    int length = (int)(strchr((*src), '.') - (*src)) + 1 + precision;
    char substring[length];
    float value;

    // We extract a substring (only the first float);
    strncpy(substring, (*src), length);
    value = atof(substring);
    // We update the pointer to remove the float we have just read
    (*src) += length;

    return value;
}

/**
 * @fn static inline int is_a_stb_packet(char *pck)
 * @brief Check if the LoRa packet reveived is a St-Bernard packet or not
 * 
 * @param pck The packet to test
 * @return 0 if the packet is a St-Bernard packet, a not null integer otherwise (strcmp result)
 * @sa strcmp 
 */
int is_a_stb_packet(char *pck);

#ifdef __cplusplus
}
#endif

#endif // _PACKET_ENCODER_H_