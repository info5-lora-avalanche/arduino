/**
 * @file saint-bernard-v2.ino
 * @author Alexandra CHATON (alexandra.chaton@etu.univ-grenoble-alpes.fr)
 * @author Romain PASDELOUP (romain.pasdeloup@etu.univ-grenoble-alpes.fr)
 * @author Thomas FRION (thomas.frion@etu.univ-grenoble-alpes.fr)
 * @brief The main part
 * @version 0.1
 * @date 2021-03-16
 * 
 * @copyright Copyright (c) 2021
 * 
 * This file contains the main functions of the chip
 */

#include "constants.h"
#include "utils.h"
#include "packet_encoder.h"

/**
 * @brief MAC address of the bluetooth module
 */
const char *MAC_BLE = "98D361FD5B13";

/**
 * @brief The current chip mode operation
 */
T_CHIP_MODE current_mode    = NOMINAL;


/**
 * @brief The counter of sent St-bernard packet
 */
static int pck_counter      = 0;

/**
 * @brief The LoRa buffer
 */
char *lbuff = (char *) calloc(MAX_DATA_SIZE, sizeof(char));

/**
 * @brief  The bluetooth buffer
 */
char *bbuff = (char *) calloc(MAX_DATA_SIZE, sizeof(char));

/**
 * @brief The current St-bernard packet we work with.
 */
st_bernart_packet_t stb_pck = NULL;

/**
 * @fn void setup()
 * @brief Sets the chip
 */
void setup() {
  	#if DEBUG // If debug is activated, we init the debug serial
  	  DEBUG_SERIAL.begin(9600);
  	  while(!DEBUG_SERIAL && millis() < 1000);
  	#endif
	  
  	// Set the baud rate of LoRa chip
  	LORA_SERIAL.begin(57600);

	//We setup all pins we will use
	pins_setup();
  	// We setup the bluetooth module
	bluetooth_setup();

	// We wait that the bluetooth module is connected to continue
	int id_connected = LOW;
	while((id_connected = digitalRead(PIN_BLE_STATE)) != HIGH) {
		blink_red_led();
	}

  	pinMode(LED_RED, OUTPUT);
	digitalWrite(LED_RED, HIGH);

	#if DEBUG
  	  DEBUG_SERIAL.println("Bluetooth connected");
  	#endif

  	// We setup the LoRa module
  	lora_chip_setup();

	// We send a bluetooth message
  	BLE_SERIAL.println("Ready to go!");
	BLE_SERIAL.flush();
	digitalWrite(LED_BUILTIN, HIGH);
}

/**
 * @fn void loop()
 * @brief The main function
 */
void loop() {
	while(read_lora_msg(lbuff) || !strstr(lbuff, RECEP_SUCCESSED));
	
	digitalWrite(LED_GREEN, LOW);
	lbuff += strlen(RECEP_SUCCESSED) + 2;

	if(is_a_stb_packet(lbuff) == 0) {
		#if DEBUG == 1
			DEBUG_SERIAL.print("LORA Received: ");
			DEBUG_SERIAL.println(lbuff);
		#endif
		lbuff += 6;
		stb_pck = decode(lbuff);
		#if DEBUG == 1
			DEBUG_SERIAL.println(stb_pck->mode);
			DEBUG_SERIAL.println(stb_pck->mac_addr);
			DEBUG_SERIAL.println(stb_pck->lng, 4);
			DEBUG_SERIAL.println(stb_pck->lat, 4);
			DEBUG_SERIAL.println(stb_pck->counter);
			DEBUG_SERIAL.println();
		#endif

		int rssi_snr = get_sig_qual();
		char msg[MAX_DATA_SIZE];
		ble_encode(stb_pck, rssi_snr, msg);

		#if DEBUG == 1
			DEBUG_SERIAL.print("MODE: ");
			DEBUG_SERIAL.println(rssi_snr & 1);
			DEBUG_SERIAL.print("VAL: ");
			DEBUG_SERIAL.println(rssi_snr >> 1);
			DEBUG_SERIAL.println(msg);
			DEBUG_SERIAL.println();
		#endif

		BLE_SERIAL.println(msg);
		BLE_SERIAL.flush();
		#if DEBUG == 1
			DEBUG_SERIAL.print("BLE Sending: ");
			DEBUG_SERIAL.println(msg);
		#endif
	}

	digitalWrite(LED_GREEN, HIGH);

	if(!read_ble_msg(bbuff)) {
		if(strlen(bbuff) > 13 && strlen(bbuff) <= 19) {
			#if DEBUG == 1
				DEBUG_SERIAL.print("BLE Received: ");
				DEBUG_SERIAL.println(bbuff);
			#endif
			digitalWrite(LED_BLUE, LOW);

			stb_pck          = create_a_packet();
			stb_pck->lng     = extract_float_from_ble_msg(&bbuff);
			stb_pck->lat     = extract_float_from_ble_msg(&bbuff);
			
			if(strlen(bbuff) > 0) {
				sscanf(bbuff, "%d", &current_mode);
				bbuff += strlen(bbuff);
			}

			stb_pck->counter = (++pck_counter);
			stb_pck->mode    = current_mode;
			set_mac_addr(stb_pck, MAC_BLE);
			
			char msg[MAX_DATA_SIZE];
			encode(stb_pck, msg);
			
			#if DEBUG 
				DEBUG_SERIAL.println();
				DEBUG_SERIAL.print(strlen(msg) / 2);
				DEBUG_SERIAL.print(' ');
				DEBUG_SERIAL.print(msg);
				DEBUG_SERIAL.println();
			#endif
			
			send_lora_msg(msg);
			digitalWrite(LED_BLUE, HIGH);
		}
	}

	if(stb_pck) {
		destroy_packet(stb_pck);
	}
}


/**
 * @fn void blink_red_led()
 * @brief Does the red led blink
 */
void blink_red_led() {
	static int fade = 5, brightness = 0;
	analogWrite(LED_RED, brightness);
	brightness += fade;
	
	if(brightness == 0 || brightness == 255) {
		fade = -fade;
	}

	delay(25);
}

/**
 * @fn void pins_setup()
 * @brief This method sets the different pins which we will use
 */
void pins_setup() {
	pinMode(PIN_BLE_CONFIG_MODE, OUTPUT);
	pinMode(PIN_BLE_STATE, INPUT);

	// We set the pin mode of leds as output (send power)
  	pinMode(LED_BUILTIN, OUTPUT);
  	pinMode(LED_GREEN, OUTPUT);
  	pinMode(LED_BLUE, OUTPUT);
  	pinMode(LED_RED, OUTPUT);

	// We change led's status
  	digitalWrite(LED_GREEN, HIGH);
  	digitalWrite(LED_BLUE, HIGH);
  	digitalWrite(LED_RED, LOW);
}

/**
 * @fn void bluetooth_setup()
 * @brief This method sets the bluetooth chip
 */
void bluetooth_setup() {
	BLE_SERIAL.begin(38400, SERIAL_8N1);
   	digitalWrite(PIN_BLE_CONFIG_MODE, HIGH);
   
   	// We set the bluetooth module name 
  	BLE_SERIAL.write("AT+NAME=St-Bernard-2\r\n");
  	delay(200);
  	digitalWrite(PIN_BLE_CONFIG_MODE, LOW);
}

/**
 * @fn void lora_chip_setup()
 * @brief This method sets the lora chip
 */
void lora_chip_setup() {
	// We reset the LoRa Chip
  	LORA_SERIAL.println("sys reset");
  	delay(200);
  	#if DEBUG
  	  DEBUG_SERIAL.println("sys reset");
  	#endif
	
	// We set the tansmitter power
  	LORA_SERIAL.print("radio set pwr ");
  	LORA_SERIAL.println(TX_POWER_14);
  	delay(100);
  	 #if DEBUG == 1
  	   DEBUG_SERIAL.print("radio set pwr ");
  	   DEBUG_SERIAL.println(TX_POWER_14);
  	 #endif

	// We set the spreading factor
  	LORA_SERIAL.print("radio set sf sf");
  	LORA_SERIAL.println(SF_10);
  	delay(100);
  	#if DEBUG == 1
  	  DEBUG_SERIAL.print("radio set sf sf");
  	  DEBUG_SERIAL.println(SF_10);
  	#endif

	// We pause the LoRaWAN stack
  	LORA_SERIAL.println("mac pause");
  	delay(100);
  	#if DEBUG == 1
  	  DEBUG_SERIAL.println("mac pause");
  	#endif

	// We set the frequency of the chip
  	LORA_SERIAL.print("radio set freq ");
  	LORA_SERIAL.println(FREQENCY);
  	delay(100);
  	#if DEBUG == 1
  	  DEBUG_SERIAL.print("radio set freq ");
  	  DEBUG_SERIAL.println(FREQENCY);
  	#endif

	// We set the coding rate of the chip
  	LORA_SERIAL.print("radio set cr ");
  	LORA_SERIAL.println(CR_8);
  	delay(100);
  	#if DEBUG == 1
  	  	DEBUG_SERIAL.print("radio set cr ");
  		DEBUG_SERIAL.println(CR_8);
  	#endif

	// We flush the LoRa buffer
	flush_lora_buffer();
}
/**
 * @fn void flush_lora_buffer()
 * @brief Clears the LoRa buffer 
 */
void flush_lora_buffer() {
	while(LORA_SERIAL.read() > -1);
}

/**
 * @fn void flush_ble_buffer()
 * @brief Clears the bluetooth buffer 
 */
void flush_ble_buffer() {
	while(BLE_SERIAL.read() > -1);
}

/**
 * @fn void send_lora_msg(char *data)
 * @brief Send hexa data through LoRa modulation
 * 
 * @param data Data to send
 */
void send_lora_msg(char *data) {
	LORA_SERIAL.print("radio tx ");
	LORA_SERIAL.println(data);

	// The flush function of SERIAL, do not clear the buffer.
	// The function wait for all bytes are sent. (see the official doc of arduino)
	if(LORA_SERIAL.availableForWrite()){
		LORA_SERIAL.flush();
	}

	#if DEBUG == 1
		while(LORA_SERIAL.available() && LORA_SERIAL.peek() != '\n') {
			DEBUG_SERIAL.write(LORA_SERIAL.read());
		}
  	  	DEBUG_SERIAL.print("LORA sending: ");
  	  	DEBUG_SERIAL.println(data);
  	#endif
}

/**
 * @fn int read_lora_msg(char *buff)
 * @brief Reads the LoRa buffer
 * 
 * @param[out] buff Pointer to out buffer 
 * @return int 0 if the operation is successful, 1 otherwise
 * 
 * The function reads the LoRa buffer. If the buffer is empty, the function will wait for the buffer to fill. 
 */
int read_lora_msg(char *buff) {
	// We listening until we receive a signal
	LORA_SERIAL.println("radio rx 0");
	delay(200); 
	flush_lora_buffer();

	// We wait until the buffer is full
	while(!LORA_SERIAL.available());
  	delay(200);
	
	// We read the buffer
	for(; LORA_SERIAL.available() > 0; buff++) {
		*buff = LORA_SERIAL.read();
	}

	return 0;
}

/**
 * @fn int read_ble_msg(char *buffer)
 * @brief Reads the bluetooth buffer
 * 
 * @param[out] buffer Pointer to out buffer
 * @return int 0 if the operation is successful, 1 otherwise
 * 
 * The function reads the bluetooth buffer. If the buffer is empty, the function will wait for the buffer to fill. 
 */
int read_ble_msg(char *buffer) {
	flush_ble_buffer();
	
	while(!BLE_SERIAL.available());
	delay(50);

	while (BLE_SERIAL.available() > 0) {
		*buffer++ = BLE_SERIAL.read();
	}

	return 0;
}

/**
 * @fn int get_sig_qual()
 * @brief Gets the signal quality of the last received lora signal
 * 
 * The function will try to get at first the value of the RSSI. If it's impossible, the function will ask for the SNR.
 * When the function has the value (RSSI or SNR), she will add a bit (at 0 poisition) to know if it is the RSSI or the SNR : 0 = RSSI, 1 = SNR 
 * 
 * @return int An integer with the value of the signal quality and a bit (bit at 0) to know if it is the RSSI or the SNR
 */
int get_sig_qual() {
	int val;
	char rssi[strlen(LORA_SYS_CMD_ERR)];

	get_lora_sys_data("rssi", rssi); // We try to get the rssi
	if(!strcmp(rssi, LORA_SYS_CMD_ERR)) {  // If we couldn't get the rssi, we get the snr

		get_lora_sys_data("snr", rssi);
		#if DEBUG == 1
			DEBUG_SERIAL.print("SNR Raw: ");
			DEBUG_SERIAL.println(rssi);
		#endif

		sscanf(rssi, "%i", &val);
		val = (val << 1) | 1; // Add a bit and set the first bit at 1 
	} else {
		sscanf(rssi, "%i", &val);
		val = (val << 1); // Add a bit and set the first bit at 0
	}

	return val;
}

/**
 * @fn void get_lora_sys_data(char *param, char *buff)
 * @brief Get a value from radio settings of the lora chip
 * 
 * @param param The value wanted
 * @param[out] buff The output buffer
 */
void get_lora_sys_data(char *param, char *buff) {
	LORA_SERIAL.print("radio get ");
	LORA_SERIAL.println(param);
	delay(100);

	for(; LORA_SERIAL.available() > 0 && LORA_SERIAL.peek() != '\n' && LORA_SERIAL.peek() != '\r'; buff++) {
		*buff = LORA_SERIAL.read();
	}
	*buff++ = '\0';

	flush_lora_buffer();
}