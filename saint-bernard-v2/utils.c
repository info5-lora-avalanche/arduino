/**
 * @file utils.c
 * @author Alexandra CHATON (alexandra.chaton@etu.univ-grenoble-alpes.fr)
 * @author Romain PASDELOUP (romain.pasdeloup@etu.univ-grenoble-alpes.fr)
 * @author Thomas FRION (thomas.frion@etu.univ-grenoble-alpes.fr)
 * @brief Useful code source
 * @version 0.1
 * @date 2021-03-16
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "utils.h"

void string_2_hex(char *i, char *o) {
    for(; *i != '\0'; i++, o += 2) {
        // We go through the input string by incrementing the pointer
        // And converting in hexa the char at i poistion.
        sprintf(o, "%02x", (*i) & 0xff);
    }
}

void hex_2_string(char *hex, char *out) {
    // We go through the input string by incrementing the pointer
    for(; *hex != '\0'; out++, hex += 2) {
        int val[1];
        // We read (as a integer) the hexa value at the hex position 
        sscanf(hex, "%02x", val);
        // The convertion (int to char) is natural thanks to ASCII
        *out = val[0];
    }
    *out = '\0'; // We stop the output string
}

float extract_float_from_ble_msg(char **src) {
    // We look for the position of the first ';' a kind of indexOf
    int length = (int) (strchr((*src), ';') - (*src)) + 1;
    // Case where there is no ';'
    length = length < 0 ? strlen((*src)) : length;
    
    char substring[length];
    // We cut the main string and get the result as a substring
    strncpy(substring, (*src), length);
    float value = atof(substring);
    // We update the pointer to remove the float we have just read
    (*src) += length;

    return value;
}