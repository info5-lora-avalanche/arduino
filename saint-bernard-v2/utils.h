/**
 * @file utils.h
 * @author Alexandra CHATON (alexandra.chaton@etu.univ-grenoble-alpes.fr)
 * @author Romain PASDELOUP (romain.pasdeloup@etu.univ-grenoble-alpes.fr)
 * @author Thomas FRION (thomas.frion@etu.univ-grenoble-alpes.fr)
 * @brief Useful elements
 * @version 0.1
 * @date 2021-03-16
 * 
 * @copyright Copyright (c) 2021
 * This file contains every useful elements like type definitions, enums, function declarations.
 */

#ifndef _UTILS_H_
#define _UTILS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <stdio.h>

/**
 * @enum T_CHIP_MODE
 * @brief List of possible operation modes
 * 
 * The chip can work in two different modes. Each mode will change the LoRa emission policy, and change the data which will be send.
 */
typedef enum {
    NOMINAL = 0x01,  /**< The NOMINAL mode sends LoRa packages according the local laws. */
    RESCUE  = 0x02,  /**< The RESCUE mode sends LoRa packages as beacon and ignores the local laws. */
    SAR     = 0x04   /**< The SAR mode will receive the RESCUE packet and help the user to localise (by radio search) the vicitm. */
} T_CHIP_MODE;

/**
 * @fn void string_2_hex(char *i, char *o)
 * @brief Convert string in hexa
 * 
 * @param[in] i Pointer to the string to convert
 * @param[out] o Pointer to the variable where the result will be stored
 */
void string_2_hex(char *i, char *o);

/**
 * @fn void hex_2_string(char *hex, char *out)
 * @brief Convert hexa to string
 * 
 * @param[in] hex Pointer to the hexa string to convert
 * @param[out] out Pointer to the string var which will contain the result
 */
void hex_2_string(char *hex, char *out);

/**
 * @fn float extract_float_from_ble_msg(char **src)
 * @brief Extracts the fist float of the string.
 *  
 * This function extracts the first float of the string.
 * The function updates de src string by removing the float extracted.
 * So if you have a string like "5.8686;345.85858". 
 * At the first call the function will return 5.8686 and the source string will be equals to "345.85858".
 * At the seconde call the function will return 345.85858 and the source string will be equals to "".
 * NOTE: This method handles only the string with ';' as separator
 * 
 * @param src Pointer of pointer to the source string
 * @return The first fload found in the string
 */
float extract_float_from_ble_msg(char **src);

#ifdef __cplusplus
}
#endif

#endif // _UTILS_H_
